import React, {Component} from 'react';
import './todolist.less';

class TodoList extends Component {

  constructor(props) {
    super(props);
    console.log('construct');
  }

  componentDidMount() {
    console.log('componentDidMount');
  }

  componentDidUpdate() {
    console.log('componentDidUpdate');
  }

  componentWillUnmount() {
    console.log('componentWillUnmount');
  }

  render() {
    console.log('render')
    return (
      <div className='todo-list'>
        <div className='todo-button-wrapper'>
          <button onClick={this.props.handleClick}>Add</button>
        </div>
        <ul>
          {this.props.list.map((item, index) => (
            <li key={index}>
              {item}
            </li>
          ))}
        </ul>
      </div>
    );
  }
}

export default TodoList;

