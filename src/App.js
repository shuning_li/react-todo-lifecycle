import React from 'react';
import './App.less';
import TodoList from './components/TodoList';

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      showTodoList: false,
      todoList: []
    };
    this.showList = this.showList.bind(this);
    this.hideList = this.hideList.bind(this);
    this.addTodo = this.addTodo.bind(this);
    this.refresh = this.refresh.bind(this);
  }

  addTodo() {
    const newItem = `List Title ${this.state.todoList.length + 1}`;
    this.setState({
      todoList: [...this.state.todoList, newItem]
    });
  }

  showList() {
    this.setState({
      showTodoList: true
    })
  }

  hideList() {
    this.setState({
      showTodoList: false,
      todoList: []
    })
  }

  refresh() {
    window.location.reload();
  }

  render() {
    return (
      <div className='App'>
        <div className='button-wrapper'>
          {this.state.showTodoList ?
            <button onClick={this.hideList}>Hide</button> :
            <button onClick={this.showList}>Show</button>}
          <button onClick={this.refresh}>Refresh</button>
        </div>
        {this.state.showTodoList && <TodoList list={this.state.todoList} handleClick={this.addTodo}/>}
      </div>
    );
  }
}

export default App;